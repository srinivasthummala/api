package com.restassured.assignment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.restassured.base.BaseTest;
import com.restassured.base.EndPoints;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class APICollection extends BaseTest {

	// 1. LIST TICKETS UNDER AN EVENT
	@Test
	public void ListTicketsUnderAnEvent() {

		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("Accept", "application/vnd.api+json");
		headers.put("Content-Type", "application/vnd.api+json");
		headers.put("Authorization", "JWT " + properties.getProperty("auth.token"));

		Response response = RestAssured.given().headers(headers).when().get(EndPoints.API_EVENTS + "/24/tickets").then()
				.log().all().and().extract().response();

		Assert.assertEquals(response.statusCode(), 200);
		Reporter.log(
				"Expected :: Status code to be 200. Actual :: status code is " + response.getStatusCode() + " <br>");
		List<String> res = response.jsonPath().get("data.id");
		Assert.assertTrue(!res.isEmpty());

	}

	// 2. TICKET FEES COLLECTION
	@Test
	public void ticketsFeeCollection() {

		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("Accept", "application/vnd.api+json");
		headers.put("Content-Type", "application/vnd.api+json");
		headers.put("Authorization", "JWT " + properties.getProperty("auth.token"));

		Response response = RestAssured.given().headers(headers).when().get(EndPoints.API_TICKET_FEES).then().log()
				.all().and().extract().response();

		Assert.assertEquals(response.statusCode(), 200);
		Reporter.log(
				"Expected :: Status code to be 200. Actual :: status code is " + response.getStatusCode() + " <br>");
	}

	// 2. ROLES COLLECTION 
	@Test
	public void rolesCollection() {

		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("Accept", "application/vnd.api+json");
		headers.put("Content-Type", "application/vnd.api+json");
		headers.put("Authorization", "JWT " + properties.getProperty("auth.token"));

		Response response = RestAssured.given().headers(headers).when().get(EndPoints.API_ROLES).then().log()
				.all().and().extract().response();

		Assert.assertEquals(response.statusCode(), 200);
		Reporter.log(
				"Expected :: Status code to be 200. Actual :: status code is " + response.getStatusCode() + " <br>");
	}

	// 3. get All orders
	@Test
	public void getAllOrders() {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/vnd.api+json");
		headers.put("Authorization", "JWT " + properties.getProperty("auth.token"));

		Response response = RestAssured.given().headers(headers).when().get(EndPoints.API_CREATE_ORDER).then().log()
				.all().and().assertThat().statusCode(200).extract().response();
		Reporter.log(
				"Expected :: Status code to be 200. Actual :: status code is " + response.getStatusCode() + " <br>");
		List<String> orderIds = response.jsonPath().get("data.id");
		Assert.assertTrue(!orderIds.isEmpty());
		Reporter.log(
				"Expected :: Orders should not be empty. Actual :: Orders displayed are " + orderIds.size() + "  <br>");
	}

}
