package com.restassured.assignment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.restassured.base.BaseTest;
import com.restassured.base.EndPoints;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class APIUserOperations extends BaseTest {

	//1. list users
	@Test(priority=0)
	public void getUsersList() {

		Response response = RestAssured.given()
											.contentType("application/vnd.api+json")
											.header("Authorization", "JWT " + properties.getProperty("auth.token"))
										.when()
											.get(EndPoints.API_LIST_USERS);
		response.then().statusCode(200);
		Reporter.log("Expected :: Status code to be 200. Actual :: status code is "+response.getStatusCode() + " <br>");
		
		JsonPath jsonPath = response.jsonPath();
		List<String> users = jsonPath.get("data");
		Assert.assertTrue(!users.isEmpty(), "Validation of users displaying");
		Reporter.log("Expected :: response should return with list of users. Actual :: "+users.size() + " Users are displyed <br>");
	}
	
	//2. single user details
	@Test (priority=1)
	public void getUserDetails(){
		
		
		List<String> usersList = RestAssured.given()
												.contentType("application/vnd.api+json")
												.header("Authorization", "JWT " + properties.getProperty("auth.token"))
											.when()
												.get(EndPoints.API_LIST_USERS)
												.jsonPath()
												.get("data.id");
		String userId = usersList.get(0);
		Response response = RestAssured.given()
											.contentType("application/vnd.api+json")
											.header("Authorization", "JWT " + properties.getProperty("auth.token"))
										.when()
											.get(EndPoints.API_LIST_USERS+"/"+userId);
		response.then().statusCode(200);
		Reporter.log("Expected :: Status code to be 200. Actual :: status code is "+response.getStatusCode() + " <br>");
		JsonPath jsonPath = response.jsonPath();
		String user = jsonPath.getString("data.id");
		Assert.assertTrue(user.equals(userId), "Validation of displaying user of ID");
		Reporter.log("Expected :: User id should be "+userId+". Actual :: User id is "+user+" <br>");
	}
	
	//3. create user
	@Test(priority=2)
	public void crreateUser() {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/vnd.api+json");
		headers.put("Authorization", "JWT " + properties.getProperty("auth.token"));
		
		String email ="John"+ new Random().nextInt(1000)+"@infostretch.com";
		
		String requestData = "{\"data\": {\"attributes\": {\"email\": \""+email+"\",\"password\": \"John1234\",\"avatar_url\": \"http://example.com/example.png\",\"first-name\": \"John\",\"last-name\": \"Doe\",\"details\": \"employee\",\"contact\": \"mobile\",\"facebook-url\": \"http://facebook.com/facebook\",\"twitter-url\": \"http://twitter.com/twitter\",\"instagram-url\": \"http://instagram.com/instagram\",\"google-plus-url\": \"http://plus.google.com/plus.google\",\"original-image-url\": \"https://cdn.pixabay.com/photo/2013/11/23/16/25/birds-216412_1280.jpg\"},\"type\": \"user\"}}";
		
		Response response = RestAssured.given()
											.headers(headers)
											.body(requestData)
										.when()
											.post(EndPoints.API_CREATE_NEW_USER)
										.then()
											.log()
											.all()
											.body("data.attributes.email", Matchers.equalTo(email))
											.extract()
											.response();
		String userId = response.jsonPath().getString("data.id");
		properties.setProperty("created.user", userId);
		
		response.then().statusCode(201);
		Reporter.log("Expected :: Status code to be 201. Actual :: status code is "+response.getStatusCode() + " <br>");
		
		Assert.assertTrue(!userId.isEmpty());
		Reporter.log("Expected :: User id should generated. Actual :: Generated user id is  " + userId + " <br>");
	}
	
	//4. update user
	@Test(dependsOnMethods={"crreateUser"}, priority=3)
	public void updateUser() {

		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/vnd.api+json");			
		headers.put("Authorization", "JWT " + properties.getProperty("auth.token"));
		String userId = properties.getProperty("created.user");
		
		String requestBody = "{\"data\": {\"attributes\": {\"avatar_url\": \"http://example.com/example.png\",\"first-name\": \"John\",\"last-name\": \"Danny\",\"details\": \"example1\",\"contact\": \"example1\",\"facebook-url\": \"http://facebook.com/facebook\",\"twitter-url\": \"http://twitter.com/twitter\",\"instagram-url\": \"http://instagram.com/instagram\",\"google-plus-url\": \"http://plus.google.com/plus.google\",\"thumbnail-image-url\": \"http://example.com/example.png\",\"small-image-url\": \"http://example.com/example.png\",\"icon-image-url\": \"http://example.com/example.png\"},\"type\": \"user\",\"id\": \""+userId+"\"}}";
		Response response = RestAssured.given()
											.headers(headers)
											.body(requestBody)
										.when()
											.patch(EndPoints.API_CREATE_NEW_USER+"/"+userId)
										.then()
											.log()
											.all()
											.and()
											.assertThat()
											.statusCode(200)
											.extract()
											.response();			
		Reporter.log("Expected :: Status code to be 200. Actual :: status code is "+response.getStatusCode() + " <br>");
		String userIdFromResponse = response.jsonPath().getString("data.id");
		Assert.assertEquals(userIdFromResponse, userId);
		Reporter.log("Expected :: User id should be "+userIdFromResponse+"  Actual :: user id is  " + userId + " <br>");
	}
	
	
	//5. delete user
	@Test(dependsOnMethods={"updateUser"}, priority=4)
	public void deleteUser() {
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/vnd.api+json");			
		headers.put("Authorization", "JWT " + properties.getProperty("auth.token"));
		String userId = properties.getProperty("created.user");
		
		Response response = RestAssured.given()
											.headers(headers)
										.when()
											.delete(EndPoints.API_CREATE_NEW_USER+"/"+userId)
										.then()
											.log()
											.all()
											.and()
											.assertThat()
											.statusCode(200)
											.and()
											.body("meta.message", Matchers.equalTo("Object successfully deleted"))
											.extract()
											.response();
			
		Reporter.log("Expected :: Status code to be 200. Actual :: status code is "+response.getStatusCode() + " <br>");
		String messageFromResponse = response.jsonPath().getString("meta.message");
		Assert.assertEquals(messageFromResponse, "Object successfully deleted");
		Reporter.log("Expected :: response message should be \"Object successfully deleted\"  Actual :: response message is  " + messageFromResponse + " <br>");
	}
	
}
