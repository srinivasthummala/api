package com.restassured.assignment;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import com.restassured.base.BaseTest;
import com.restassured.base.EndPoints;

import io.restassured.RestAssured;

public class EventInvoices extends BaseTest {

	//1.Get a list of event invoices. 
	@Test
	public void listEventInvoices(){
		
		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("Accept", "application/vnd.api+json");
		headers.put("Content-Type", "application/vnd.api+json");
		headers.put("Authorization","JWT "+ properties.getProperty("auth.token"));
		
		RestAssured.given().headers(headers).when().get(EndPoints.API_EVENT_INVOICES).then().log().all().and().statusCode(200);
		
	}
}
