package com.restassured.assignment;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.restassured.base.BaseTest;
import com.restassured.base.EndPoints;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class EventOperations extends BaseTest {

	// 1. list all events
	@Test
	public void getAllEvents() {

		List<Header> listHeaders = new ArrayList<Header>();
		listHeaders.add(new Header("Content-Type", "application/vnd.api+json"));
		listHeaders.add(new Header("Authorization", "JWT " + properties.getProperty("auth.token")));
		Headers headers = new Headers(listHeaders);
		RestAssured.given()
						.headers(headers)
					.when()
						.get(EndPoints.API_EVENTS)
					.then()
					.log()
					.all()
					.and()
					.statusCode(200)
					.body("data.size()", Matchers.greaterThan(0));
	}

	// 2. create event
	@Test
	public void createEvent() {
		String eventName = null;
		try {

			DocumentContext context = JsonPath.parse(
					new File(
							"C:\\Users\\srinivas.thummala\\git\\api\\RestAssured\\resourses\\testdata\\CreateEvent.json"),
					Configuration.defaultConfiguration());
			eventName = context.read("$.data.attributes.name");
			System.out.println(eventName);
		} catch (IOException e) {
			e.printStackTrace();
		}

		List<Header> listHeaders = new ArrayList<Header>();
		listHeaders.add(new Header("Content-Type", "application/vnd.api+json"));
		listHeaders.add(new Header("Authorization", "JWT " + properties.getProperty("auth.token")));
		Headers headers = new Headers(listHeaders);
		Response response = RestAssured.given()
											.headers(headers)
											.body(new File("./resourses/testdata/CreateEvent.json"))
										.when()
											.post(EndPoints.API_EVENTS)
										.then()
											.log()
											.all()
											.and()
											.statusCode(201)
											.body("data.size()", Matchers.greaterThan(0))
											.and()
											.body("data.attributes.name", Matchers.equalTo(eventName)).extract().response();
		String eventId = JsonPath.read(response.asString(), "$.data.id");
		properties.setProperty("event.id", eventId);
	}

	// 3. update event
	@Test(dependsOnMethods = { "createEvent" })
	public void updateEvent() {
		String eventId = properties.getProperty("event.id");
		DocumentContext updateEvent = null;
		String eventName = null;
		try {
			DocumentContext context = JsonPath.parse(
					new File(
							"C:\\Users\\srinivas.thummala\\git\\api\\RestAssured\\resourses\\testdata\\UpdateEvent.json"),
					Configuration.defaultConfiguration());
			updateEvent = context.set("$.data.id", eventId);
			eventName = context.read("$.data.attributes.name");
		} catch (IOException e) {
			e.printStackTrace();
		}

		RestAssured.given()
					.header("Content-Type", "application/vnd.api+json")
					.header("Authorization", "JWT " + properties.getProperty("auth.token"))
					.body(updateEvent.jsonString())
				.when()
					.patch(EndPoints.API_EVENTS + "/" + eventId)
				.then()
					.log()
					.all()
					.and()
					.statusCode(200)
					.and()
					.body("data.attributes.name", Matchers.equalTo(eventName));
	}

	// 4. delete event
	@Test(dependsOnMethods = { "createEvent" })
	public void getEvent() {
		String eventId = properties.getProperty("event.id");
		RestAssured.given()
						.header("Content-Type", "application/vnd.api+json")
						.header("Authorization", "JWT " + properties.getProperty("auth.token"))
					.when()
						.get(EndPoints.API_EVENTS + "/" + eventId)
					.then()
						.log()
						.all()
						.and()
						.statusCode(200)
						.and()
						.body("data.id", Matchers.equalTo(eventId));
	}

	// 5. delete event
	@Test(dependsOnMethods = { "getEvent" }, priority=1)
	public void deleteEvent() {
		String eventId = properties.getProperty("event.id");
		RestAssured.given()
						.header("Content-Type", "application/vnd.api+json")
						.header("Authorization", "JWT " + properties.getProperty("auth.token"))
					.when()
						.delete(EndPoints.API_EVENTS + "/" + eventId)
					.then()
						.log()
						.all()
						.statusCode(200)
						.and()
						.body("meta.message", Matchers.equalTo("Object successfully deleted"));
	}

}
