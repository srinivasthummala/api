package com.restassured.assignment;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;
import org.testng.annotations.Test;

import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.restassured.base.EndPoints;

import io.restassured.RestAssured;

public class TracksCollection extends EventOperations {

	// 1.Get a list of event invoices.
	@Test(dependsOnMethods = { "createEvent" }, priority =0)
	public void validateCreateTrack() {

		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("Accept", "application/vnd.api+json");
		headers.put("Content-Type", "application/vnd.api+json");
		headers.put("Authorization", "JWT " + properties.getProperty("auth.token"));

		DocumentContext trackBody = null;
		try {

			DocumentContext context = JsonPath.parse(
					new File(
							"C:\\Users\\srinivas.thummala\\git\\api\\RestAssured\\resourses\\testdata\\CtreateTrack.json"),
					Configuration.defaultConfiguration());
			trackBody = context.set("$.data.relationships.event.data.id", properties.getProperty("event.id"));
			System.out.println(trackBody.jsonString());
		} catch (IOException e) {
			e.printStackTrace();
		}

		Object path = RestAssured.given()
									.headers(headers)
									.body(trackBody.jsonString())
								.when()
									.post(EndPoints.API_TRACKS)
								.then()
									.log()
									.all()
									.and()
									.statusCode(201)
									.extract()
									.path("data.id");
		properties.setProperty("track.id", path.toString());

	}

	// 2.Get a single track.
	@Test(dependsOnMethods = { "validateCreateTrack" }, priority =0)
	public void validateTrackDetails() {

		Map<String, Object> headers = new HashMap<String, Object>();
		headers.put("Accept", "application/vnd.api+json");
		headers.put("Content-Type", "application/vnd.api+json");
		headers.put("Authorization", "JWT " + properties.getProperty("auth.token"));

		String trackName = null;
		String trackDesc = null;
		try {

			DocumentContext context = JsonPath.parse(
					new File(
							"C:\\Users\\srinivas.thummala\\git\\api\\RestAssured\\resourses\\testdata\\CtreateTrack.json"),
					Configuration.defaultConfiguration());
			trackName = context.read("$.data.attributes.name");
			trackDesc = context.read("$.data.attributes.description");

		} catch (IOException e) {
			e.printStackTrace();
		}

		RestAssured.given()
						.headers(headers)
					.when()
						.get(EndPoints.API_TRACKS + "/" + properties.getProperty("track.id"))
					.then()
						.log()
						.all()
						.and()
						.statusCode(200)
						.and()
						.body("data.attributes.name", Matchers.equalTo(trackName))
						.body("data.attributes.description", Matchers.equalTo(trackDesc));
	}

	// 3. update event
	@Test(dependsOnMethods = { "validateTrackDetails" })
	public void updateTrack() {

		String trackId = properties.getProperty("track.id");
		DocumentContext updateTrack = null;
		String trackName = null;
		String trackDesc = null;
		try {
			DocumentContext context = JsonPath.parse(
					new File(
							"C:\\Users\\srinivas.thummala\\git\\api\\RestAssured\\resourses\\testdata\\UpdateTrack.json"),
					Configuration.defaultConfiguration());
			updateTrack = context.set("$.data.id", trackId);
			trackName = context.read("$.data.attributes.name");
			trackDesc = context.read("$.data.attributes.description");
		} catch (IOException e) {
			e.printStackTrace();
		}

		RestAssured.given()
						.header("Content-Type", "application/vnd.api+json")
						.header("Authorization", "JWT " + properties.getProperty("auth.token"))
						.body(updateTrack.jsonString())
					.when()
						.patch(EndPoints.API_TRACKS + "/" + trackId)
					.then()
						.log()
						.all()
						.and()
						.statusCode(200)
						.and()
						.body("data.attributes.name", Matchers.equalTo(trackName))
						.body("data.attributes.description", Matchers.equalTo(trackDesc));
	}

	// 4. delete event
	@Test(dependsOnMethods = { "updateTrack" })
	public void deleteTrack() {
		String tracktId = properties.getProperty("track.id");
		RestAssured.given()
						.header("Content-Type", "application/vnd.api+json")
						.header("Authorization", "JWT " + properties.getProperty("auth.token"))
					.when()
						.delete(EndPoints.API_TRACKS + "/" + tracktId)
					.then()
						.log()
						.all()
						.statusCode(200)
						.and()
						.body("meta.message", Matchers.equalTo("Object successfully deleted"));
	}

	// 5. delete event
	@Test(dependsOnMethods = { "validateCreateTrack" })
	public void tracksUnderEvent() {
		String eventId = properties.getProperty("event.id");
		RestAssured.given()
						.header("Content-Type", "application/vnd.api+json")
						.header("Authorization", "JWT " + properties.getProperty("auth.token"))
					.when()
						.get(EndPoints.API_EVENTS+ "/" + eventId+"/tracks")
					.then()
						.log()
						.all()
						.statusCode(200)
						.and()
						.body("data.size()", Matchers.greaterThan(0));
	}
	
}
