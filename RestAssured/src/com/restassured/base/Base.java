package com.restassured.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class Base {

	public static Properties properties;
	static{
		try {
			properties = new Properties();
			properties.load(new FileInputStream(System.getProperty("user.dir") + "/resourses/application.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
