package com.restassured.base;

import javax.json.Json;
import javax.json.JsonObject;

import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class BaseTest extends Base {
	
	@BeforeClass
	public void Test(){
		RestAssured.baseURI = EndPoints.BASE_URL;

//		String requestBody = "{ \"email\": \"admin@mailinator.com\",  \"password\": \"admin123#\"}";
		JsonObject jsonCredentials = Json.createObjectBuilder().add("email", "admin@mailinator.com").add("password", "admin123#").build();

		Response response = RestAssured.given().contentType(ContentType.JSON).body(jsonCredentials.toString()).post(EndPoints.API_AUTHENTICATION);
		System.out.println(response.asString());
		MatcherAssert.assertThat("Validate status code 200", response.getStatusCode(), Matchers.equalTo(200));
		properties.setProperty("auth.token", response.getBody().path("access_token").toString());
		
	}
}
