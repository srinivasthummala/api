package com.restassured.base;

public class EndPoints {

	public static final String BASE_URL = "http://qe.events.infostretch.com/api";

	public static final String API_AUTHENTICATION = "/v1/auth/login";
	public static final String API_LIST_USERS = "/v1/users";
	public static final String API_CREATE_NEW_USER = "/v1/users";
	public static final String API_LIST_USER = "/v1/users";
	public static final String API_CREATE_ORDER = "/v1/orders";
	public static final String API_DELETE_ORDER = "/v1/orders";
	public static final String API_EVENTS = "/v1/events";
	public static final String API_EVENT_INVOICES = "/v1/event-invoices";
	public static final String API_TRACKS = "/v1/tracks";
	public static final String API_TICKETS = "/v1/tickets";
	public static final String API_TICKET_FEES = "/v1/ticket-fees";
	public static final String API_ROLES = "/v1/roles";
}
